﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using SimpleWheaterStation.DataModel;

namespace SimpleWheaterStation.API.Controllers
{
    [DisableCors]
    [Route("[controller]")]
    [ApiController]
    public class WeathersController : ControllerBase
    {
        private readonly AppDbContext _context;

        public WeathersController(AppDbContext context)
        {
            _context = context;
        }

        [HttpGet("GetWeather/{startDate}/{endDate}")]
        public IActionResult Weathers(DateTime startDate, DateTime endDate)
        {         
            var weathersInfo = _context.Weathers
                                         .Where(wm => wm.Date >= startDate && wm.Date <= endDate)
                                         .Select(
                                             weathersInformation => new
                                             {
                                                 id = weathersInformation.WeatherModelId,
                                                 date = weathersInformation.Date,
                                                 airHumidity = weathersInformation.AirHumidity,
                                                 temperature = weathersInformation.Temperature,
                                                 airPressure = weathersInformation.AirPressure                                 
                                             });

            return Ok(weathersInfo);
        }

        [HttpPost("AddWeatherInfo")]
        public IActionResult AddWeatherInfo(WeatherModel weatherInfo)
        {
            var newWeatherInfo = new WeatherModel
            {
                Date = weatherInfo.Date,
                AirHumidity = weatherInfo.AirHumidity,
                Temperature = weatherInfo.Temperature,
                AirPressure = weatherInfo.AirPressure
            };

            var createNewWeatherInfo = _context.Weathers.Add(newWeatherInfo);
            _context.SaveChanges();
            return new ObjectResult(createNewWeatherInfo);
        }
    }
}
