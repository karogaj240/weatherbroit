import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http'; 
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WeatherStationBaseLayoutComponent } from './weather-station-base-layout/weather-station-base-layout.component';
import { WeatherStationBaseHeaderComponent } from './weather-station-base-layout/weather-station-base-header/weather-station-base-header.component';
import { WeatherStationBaseFooterComponent } from './weather-station-base-layout/weather-station-base-footer/weather-station-base-footer.component';
import { WeatherStationInfoOnePointComponent } from './weather-station-info-one-point/weather-station-info-one-point.component';
import {NgxMaskModule} from 'ngx-mask';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    WeatherStationBaseLayoutComponent,
    WeatherStationBaseHeaderComponent,
    WeatherStationBaseFooterComponent,
    WeatherStationInfoOnePointComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgxMaskModule.forRoot(),
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
