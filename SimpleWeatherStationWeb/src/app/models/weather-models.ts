export interface IWeatherModel {
  id: number;
  date: Date;
  airHumidity: number;
  temperature: number;
  airPressure: number;
}
