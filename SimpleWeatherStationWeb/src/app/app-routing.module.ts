import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WeatherStationBaseLayoutComponent } from './weather-station-base-layout/weather-station-base-layout.component';

const routes: Routes = [
  {
    path: '',
    component: WeatherStationBaseLayoutComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
