import { Component, OnInit, ViewChild, ElementRef, ViewEncapsulation } from '@angular/core';
import { WeathersService } from '../services/weathers.service';
import { IWeatherModel } from '../models/weather-models';

@Component({
  selector: 'app-weather-station-base-layout',
  templateUrl: './weather-station-base-layout.component.html',
  styleUrls: ['./weather-station-base-layout.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class WeatherStationBaseLayoutComponent implements OnInit {

  @ViewChild('weahter-info') weatherInfo: ElementRef;
  weathers: IWeatherModel[];
  startDate: Date= new Date("1990-01-01");
  endDate: Date = new Date("3000-01-31");
  constructor( 
    private weathersService: WeathersService
  ) { }

  ngOnInit() {
    this.getWeathers();
  }

  getWeathers(){
    this.weathersService.getWeathers(this.startDate, this.endDate)
      .subscribe(result => {
        this.weathers = result;
      })
  }

  getStartDate(event){
    this.startDate=event;
    this.getWeathers();
  }

  getEndDate(event){
    this.endDate=event;    
    this.getWeathers();
  }
  addBackground(){
    this.weathers.forEach(element => {
      
    });
  }
}
