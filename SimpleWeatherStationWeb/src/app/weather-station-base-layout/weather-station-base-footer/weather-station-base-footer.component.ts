import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-weather-station-base-footer',
  templateUrl: './weather-station-base-footer.component.html',
  styleUrls: ['./weather-station-base-footer.component.scss']
})
export class WeatherStationBaseFooterComponent implements OnInit {

  constructor() { }

  mailKarolina: string = "karolinagajda96@gmail.com";
  mailMateusz: string = "mateuszszwedka96@gmail.com"

  ngOnInit() {
  }

}
