import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { NgxMaskModule } from 'ngx-mask'
import * as moment from 'moment';

@Component({
  selector: 'app-weather-station-base-header',
  templateUrl: './weather-station-base-header.component.html',
  styleUrls: ['./weather-station-base-header.component.scss']
})
export class WeatherStationBaseHeaderComponent implements OnInit {

  startDate: string;
  endDate: string;

  @Output() startDateEvent = new EventEmitter();
  @Output() endDateEvent = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  sendRangeDates(){
    if(this.startDate<this.endDate){
      this.startDateEvent.emit(moment(this.startDate, 'YYYY-mm-dd'));
      this.endDateEvent.emit(this.endDate);
    }
  }
}
