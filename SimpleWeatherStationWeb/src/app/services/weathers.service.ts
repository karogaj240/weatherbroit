import { Injectable } from '@angular/core';
import { IWeatherModel } from '../models/weather-models';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class WeathersService {

  constructor(
    private httpClient: HttpClient,
  ) { }

  getWeathers(startDate: Date, endDate: Date): Observable<IWeatherModel[]> {
    const formatStartDate = moment(startDate).toISOString();
    const formatEndDate = moment(endDate).toISOString();
    return this.httpClient.get<IWeatherModel[]>(
      // 'http://localhost:55152/weathers/getweather' + '/' + formatStartDate + '/' + formatEndDate
      'https://simplewheaterstationapi.azurewebsites.net/weathers/getweather' + '/' + formatStartDate + '/' + formatEndDate
    );
  }
}
