import { Component, OnInit, Input, ElementRef, ViewChild } from '@angular/core';
import { IWeatherModel } from '../models/weather-models';

@Component({
  selector: 'app-weather-station-info-one-point',
  templateUrl: './weather-station-info-one-point.component.html',
  styleUrls: ['./weather-station-info-one-point.component.scss']
})
export class WeatherStationInfoOnePointComponent implements OnInit {


  @Input() weather: IWeatherModel;
  constructor() { }

  ngOnInit() {
  }
}
