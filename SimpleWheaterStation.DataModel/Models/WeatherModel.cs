﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SimpleWheaterStation.DataModel
{
    public class WeatherModel
    {
        [Key]
        public int WeatherModelId { get; set; }

        public DateTime Date { get; set; }

        public int AirHumidity { get; set; }

        public int Temperature { get; set; }

        public float AirPressure { get; set; }
    }
}
